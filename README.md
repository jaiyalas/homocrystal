# homocrystal

- [Website](https://lambdaww.notion.site/Homocrystal-5aa36a1c099341898e11f59b62acb1a0)

## Board

### ESP32

| Project  | Build | Upload | Fully Functional |
| -------- | ----- | ------ | ---------------- |
| /blink   | ✅    | ✅     | ✅               |
| /wifi    | ✅    | ✅     | ✅               |
| /ntp     | ✅    | ✅     | ✅               |
| /clock   | ✅    | ✅     |                  |
| /ssd1306 | ✅    | ✅     | ✅               |

### Arduino

TBD

### SigmaStar

TBC

## Installed Libs

Updated with

```
> arduino-cli lib list > library.log
```

