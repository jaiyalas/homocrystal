/* #### MH-Z14A - CO2 detector via PWM #### */

#include "esp_mac.h"

// ## interface: PWM
// PWM --> G0
const int pwmPin = 0;

void preheat() {
  Serial.println("System initializing..");
  delay(3000);
  Serial.println("Preheating..");
  int i = 20;
  while (i > 0){
    int t = i * 10;
    Serial.print(t);
    Serial.println("s remaining");
    delay(10000);
    i--;
  }
}

void setup() {
  pinMode(pwmPin, INPUT_PULLUP);
  Serial.begin(115200);
  delay(3000);
  preheat();
  uint8_t mac[6];
  esp_read_mac(mac, ESP_MAC_BT);

  Serial.print("MAC Address: ");
  for (int i = 0; i < 6; i++) {
    if (mac[i] < 16) {
      Serial.print("0");
    }
    Serial.print(mac[i], HEX);
    if (i < 5) Serial.print(":");
  }
  Serial.println("\n");
}

void loop() {
  int ppm_PWM = gas_concentration_PWM();
  Serial.println(ppm_PWM);
  // sleep for 1 minute
  delay(60000);
}

// ## function: obtain data from PWM
int gas_concentration_PWM() {
  while (digitalRead(pwmPin) == LOW) {};
  long t0 = millis();
  while (digitalRead(pwmPin) == HIGH) {};
  long t1 = millis();
  while (digitalRead(pwmPin) == LOW) {};
  long t2 = millis();
  long th = t1-t0;
  long tl = t2-t1;
  long ppm = 5000L * (th - 2) / (th + tl - 4);
  while (digitalRead(pwmPin) == HIGH) {};
  delay(10);
  return int(ppm);
}

