# MH-Z14a

CO2 sensor on ESP32-S2 (WROVER-I)

## Notes

Using PWM cause analog on ESP32 is tricky to translate.

## Local recalibration

Recalibrated at 25°02'26.8"N 121°26'60.0"E at 01 April 2024 15:30 (GMT+8).

## Ref

### CO2

400ppm = outdoor (baseline)
650ppm = indoor with circulation
850ppm = feeling stuffy
1000ppm = standard indoor
2500ppm = negative influrance
5000ppm = danger
