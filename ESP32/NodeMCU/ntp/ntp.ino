#include "WiFi.h"
#include "WiFiUdp.h"
#include "NTPClient.h"
#include "TimeLib.h"

// wifi setup
const char* ssid = "Meat1029";
const char* password = "hhcu1029";

// NTP connector
WiFiUDP ntpUDP;
// [UTC+8]: 8*60*60 = 28800
NTPClient timeClient(ntpUDP, "asia.pool.ntp.org", 28800, 60000);

// time var
char Time[ ] = "TIME:00:00:00";
char Date[ ] = "DATE:00/00/2000";
byte last_second, second_, minute_, hour_, day_, month_;
int year_;

// ##== setup ==## //
void setup() {
  Serial.begin(115200);

  WiFi.begin(ssid,password);
  Serial.println("Connecting to WiFi..");
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  Serial.println("Connected to the WiFi network");
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  timeClient.begin();
}

// ##== loop ==## //
void loop() {

  timeClient.update();
  unsigned long t = timeClient.getEpochTime();
  second_ = second(t);
  if (last_second != second_) {
    //
    minute_ = minute(t);
    hour_   = hour(t);
    day_    = day(t);
    month_  = month(t);
    year_   = year(t);
    //
    Time[12] = second_ % 10 + 48;
    Time[11] = second_ / 10 + 48;
    Time[9]  = minute_ % 10 + 48;
    Time[8]  = minute_ / 10 + 48;
    Time[6]  = hour_   % 10 + 48;
    Time[5]  = hour_   / 10 + 48;
    //
    Date[5]  = day_   / 10 + 48;
    Date[6]  = day_   % 10 + 48;
    Date[8]  = month_  / 10 + 48;
    Date[9]  = month_  % 10 + 48;
    Date[13] = (year_   / 10) % 10 + 48;
    Date[14] = year_   % 10 % 10 + 48;
    //
    Serial.println(Time);
    Serial.println(Date);
    //
    last_second = second_;
  }
  delay(998);
}