# SSD 1306

## connections

- 7 pin
  - GND := GND
  - VCC := 3v3
  - D0 = clk := GPIO 18 (VSPI_CLK)
  - D1 = MOSI := GPIO 23 (VSPI_MOSI)
  - RES = reset := GPIO 17 (?)
  - DC (data/cmd slect) := GPIO 16 (?)
  - CS (chip select) := GPIO 5 (VSPI_CS0)

## SPI on ESP32

There are 4 SPI on ESP32

- SPI0 = dedicated to the flash cache
- SPI1 = as above
- SPI2 = HSPI - free to use
- SPI3 = VSPI - free to use

Each of them has 4 pins

- SCLK (serial clock)
- MOSI (master output, slave input)
- MISO (master input, slave output)
- SS (slave select) | CS (chip select)

## Reference

- https://www.electronicshub.org/esp32-oled-display/
