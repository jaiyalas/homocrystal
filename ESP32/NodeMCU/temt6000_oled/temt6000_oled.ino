#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define OLED_MOSI   23 // VSPI_MOSI
#define OLED_CLK    18 // VSPI_CLK
#define OLED_CS     5  // VSPI_CS0
#define OLED_DC     16 // data/cmd select
#define OLED_RESET  17
Adafruit_SSD1306 display( SCREEN_WIDTH
                        , SCREEN_HEIGHT
                        , OLED_MOSI
                        , OLED_CLK
                        , OLED_DC
                        , OLED_RESET
                        , OLED_CS);

#define TEMT6000    34

void setup() {
  Serial.begin(115200);

  // TEMT6000 pin mode setup
  pinMode(TEMT6000, INPUT);

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  display.display();
  display.cp437(true);
  delay(500);
  display.clearDisplay();
  delay(2000);

}

void loop() {

  // Light Reading - TEMT6000
  analogReadResolution(10);

  // Convert reading to VOLTS
  float volts =
    analogRead(TEMT6000) * 5 / 1024.0;
  //Reading to Percent of Voltage
  float VoltPercent =
    analogRead(TEMT6000) / 1024.0 * 100;

  //Conversions from reading to LUX
  float amps = volts / 10000.0;  // em 10,000 Ohms
  float microamps = amps * 1000000; // Convert to Microamps
  float lux = microamps * 2.0; // Convert to Lux */
  delay(500);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0,0);

  display.print("LUX - ");
  display.print(lux);
  display.println(" lx");
  //
  display.print(VoltPercent);
  display.println("%");
  display.print(volts);
  display.println(" volts");
  //
  display.print(amps);
  display.println(" amps");
  //
  display.print(microamps);
  display.println(" microamps");
  //
  // display.print(F("0x"));
  // display.println(0xDEADBEEF, HEX);
  display.display();
  delay(2000);
}
