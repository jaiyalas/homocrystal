/* ##############################################################
> work only for 5.56in e-paper of waveshare
> resolution = 600 * 448
############################################################## */

// include for NTP
#include "WiFi.h"
#include "WiFiUdp.h"
#include "NTPClient.h"
#include "TimeLib.h"

// include for e-ink
#include "DEV_Config.h"
#include "EPD_5in65f.h"
#include "GUI_Paint.h"
#include <stdlib.h>

// wifi setup
const char* ssid = "Meat1029";
const char* password = "hhcu1029";

// NTP connector
WiFiUDP ntpUDP;
// [UTC+8]: 8*60*60 = 28800
NTPClient timeClient(ntpUDP, "asia.pool.ntp.org", 28800, 60000);

// time var
char Time[] = "00:00:00";   // width = 136px for font24
char Date[] = "00/00/2000"; // width = 170px for font24
byte last_second, second_, minute_, hour_, day_, month_;
int year_;

int ctr;

// ##### setup ##### //
// ----------------- //
void setup()
{
  //
  Serial.begin(115200);
  ctr = 0;

  //
  Serial.println("Connecting to WiFi..");
  WiFi.begin(ssid,password);
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to the WiFi network");
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  //
  timeClient.begin();
  unsigned long t = timeClient.getEpochTime();
  second_ = second(t);
  delay(3000);

  // EPD init
  Serial.println("EPD_5in65F init\r\n");
  DEV_Module_Init();
  Serial.println("e-Paper Init and Clear...\r\n");
  EPD_5IN65F_Init();
  EPD_5IN65F_Clear(EPD_5IN65F_WHITE);
  delay(1000);
  Serial.println("Delay 3s..");
  delay(3000);
}

/* The main loop -------------------------------------------------------------*/
void loop(){
  if(ctr >= 144){ // 144 = 5m * 12h
    Serial.printf("Sleep...\r\n");
    EPD_5IN65F_Sleep();
  } else {
    ctr += 1;
    Serial.println("-----");
    Serial.print("CTR:");
    Serial.println(ctr);
    //
    timeClient.update();
    unsigned long t = timeClient.getEpochTime();
    second_ = second(t);
    if (last_second != second_) {
      //
      minute_ = minute(t);
      hour_   = hour(t);
      day_    = day(t);
      month_  = month(t);
      year_   = year(t);
      //
      Time[0]  = hour_   / 10 + 48;
      Time[1]  = hour_   % 10 + 48;
      Time[3]  = minute_ / 10 + 48;
      Time[4]  = minute_ % 10 + 48;
      Time[6] = second_ / 10 + 48;
      Time[7] = second_ % 10 + 48;
      //
      Date[0]  = day_   / 10 + 48;
      Date[1]  = day_   % 10 + 48;
      Date[3]  = month_  / 10 + 48;
      Date[4]  = month_  % 10 + 48;
      Date[8] = (year_   / 10) % 10 + 48;
      Date[9] = year_   % 10 % 10 + 48;
      //
      Serial.println(Time);
      Serial.println(Date);
      //
      last_second = second_;
    }

    // canva
    UBYTE *canva;
    UDOUBLE Imagesize = (EPD_5IN65F_WIDTH / 2) * EPD_5IN65F_HEIGHT;
    Serial.printf("Imagesize %d\r\n",Imagesize);
    if((canva = (UBYTE *)malloc(Imagesize/2)) == NULL) {
      Serial.printf("Failed to apply for black memory...\r\n");
      while(1);
    }
    Paint_NewImage( canva
                  , EPD_5IN65F_WIDTH
                  , EPD_5IN65F_HEIGHT / 2
                  , 0, EPD_5IN65F_WHITE);
    Paint_SetScale(7);
    // Drawing
    Paint_Clear(EPD_5IN65F_WHITE);
    // show counter
    Paint_DrawNum(500, 50, ctr, &Font24
                 , EPD_5IN65F_WHITE, EPD_5IN65F_BLUE);
    Paint_DrawNum(500, 180, ctr, &Font12
                 , EPD_5IN65F_BLACK, EPD_5IN65F_WHITE);
    // show clock
    Paint_DrawCircle( 300, 100, 50
                    , BLACK, DOT_PIXEL_3X3, DRAW_FILL_EMPTY);
    // Paint_DrawCircle( 300, 100, 45
    //                 , EPD_5IN65F_GREEN, DOT_PIXEL_1X1, DRAW_FILL_FULL);
    // show digital date/time
    Paint_DrawString_EN( 30, 150, Date, &Font24
                       , EPD_5IN65F_GREEN, EPD_5IN65F_WHITE);
    Paint_DrawString_EN( 370, 150, Time, &Font24
                       , EPD_5IN65F_YELLOW, EPD_5IN65F_RED);
    EPD_5IN65F_Display_part(canva, 0, 0, 600, 224);
    // EPD_5IN65F_Display(canva);
    delay(500);
    free(canva);
    canva = NULL;
    delay(5*60*1000);
  }
}
