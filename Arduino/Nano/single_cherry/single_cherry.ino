/* ----------------------------------
   https://learn.sparkfun.com/tutorials/cherry-mx-switch-breakout-hookup-guide
   https://github.com/sparkfun/Cherry_MX_Switch_Breakout
   https://github.com/sparkfun/CherryMX-Chord-Keyer/blob/master/Firmware/Keyer.ino
   https://github.com/DylanMeeus/ArduinoNano_KeyInput/blob/master/NanoCode_pde/NanoCode_pde.ino
 */
// #include <keyboard.h>

// max debounce number
#define MAX_DB (3)

/*
diode (二極體):= [anode (+) 陽/正極] -> [cathode (-) 陰/負極]
 */
//
// /!\ row = cathode
//     col = anode
// pin: switch row (2)
static const uint8_t btnRowPins = 8;
// pin: switch column (1)
static const uint8_t btnColPins = 11;
// pin: LED row (-)
static const uint8_t ledRowPins = 2;
// pin: LED cols (+)
static const uint8_t ledColPins = 5;

// variables
int  key;     // key state
bool ledbuf;  // LED buffer
int  dbc;     // debounce counter

void setup() {
  Serial.begin(115200);

  // #1 setup switch.
  pinMode(btnRowPins, OUTPUT);
  // ##1.1 LOW as active, with nothing selected by default
  digitalWrite(btnRowPins, HIGH);
  // ##1.2 Pulled high through resistor.
  //       Will be LOW when active
  pinMode(btnColPins, INPUT_PULLUP);
  // #2 setup LED
  pinMode(ledRowPins, OUTPUT);
  digitalWrite(ledRowPins, HIGH);
  pinMode(ledColPins, OUTPUT);
  digitalWrite(ledColPins, LOW);
  // #3 setup var
  key = 0;
  dbc = 0;
  ledbuf = 0;
}

void loop() {
  //
  digitalWrite(btnRowPins, LOW);
  digitalWrite(ledRowPins, LOW);
  // turn LED on
  if(ledbuf){
    digitalWrite(ledColPins, HIGH);
  }
  // if switch pressed?
  if(digitalRead(btnColPins) == LOW){
    if (dbc < MAX_DB){
      dbc++;
      if(dbc = MAX_DB){
        Serial.print("+");
        ledbuf = 1;
      }
    }
  } else { // switch released
    if (dbc > 0){
      dbc--;
      if(dbc == 0){
        Serial.print("-");
        ledbuf = 0;
      }
    }
  }
  // de-select by setting cathode to HIGH
  digitalWrite(btnRowPins, HIGH);
  digitalWrite(ledRowPins, HIGH);
  //turn LDE off
  digitalWrite(ledColPins, LOW);
  // // //
  // if(digitalRead(9) == 0){
  //   delay(100);
  //   if(digitalRead(9)==0){key=key+1;}
  //   keyboard.write('A');
  //   key=0;
  //   delay(100);
  // }
}
