# Single Cherry MX switch with LED

## wire

```
(+) -- P5/D5
(-) -- P2/D2
(1) -- P11/D11
(2) -- P8/D8
```

## build and burn

Assuming:

- using Arduino Nano board `arduino:avr:nano`
- the USB port is `/dev/ttyUSB0`

To compile and upload:
```
> arduino-cli compile . --fqbn 'arduino:avr:nano'
> arduino-cli upload . -p /dev/ttyUSB0 --fqbn 'arduino:avr:nano'
```

Don't forget to set up permission for uploading
```
> sudo chmod a+rw /dev/ttyUSB0
```
